package netlight.sz_workshop.persistence;

import java.net.UnknownHostException;

import netlight.sz_workshop.SampleClass;

import org.jongo.Jongo;
import org.jongo.MongoCollection;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

public class MongoDB {
	
	public static void get() throws UnknownHostException{
		DB db = new MongoClient().getDB("dbname");

		Jongo jongo = new Jongo(db);
		MongoCollection scc = jongo.getCollection("friends");

		Iterable<SampleClass> s = scc.find("{a: 1}").as(SampleClass.class);
		SampleClass one = scc.findOne("{a: 1}").as(SampleClass.class);
	}

}
