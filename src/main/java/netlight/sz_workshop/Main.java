package netlight.sz_workshop;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import netlight.sz_workshop.jsonparser.Jackson;
import netlight.sz_workshop.jsonparser.SimpleJSON;

/**
 * Hands on stuff für den Android Workshop
 * @author mazi
 *
 */
public class Main 
{
    public static void main( String[] args ) throws JsonGenerationException, JsonMappingException, IOException
    {
    	jsonParsing();
    }

	private static void jsonParsing() throws JsonGenerationException,
			JsonMappingException, IOException {
		SampleClass sc = new SampleClass(9, 8., new String[]{"Sueddeutsche", "Zeitung"}, new SampleClass());
		
		
		System.out.println("[Simple JSON - Lightweight JSON Parsing]");
    	SimpleJSON.encode();
        SimpleJSON.decode();
        
       
        System.out.println("\n[Jackson - Heavyweight and sophisticated JSON Parsing]");
        System.out.printf("Encoding: %s\n", sc.getClass().getSimpleName());
        String json = Jackson.encode(sc);
        System.out.printf("Decoding: %s\n", json);
        SampleClass sc_parsed = Jackson.decode(json);
	}
}
