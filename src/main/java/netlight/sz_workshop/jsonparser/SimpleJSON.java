package netlight.sz_workshop.jsonparser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class SimpleJSON {
	
	
	@SuppressWarnings("unchecked")
	/**
	 * https://code.google.com/p/json-simple/wiki/EncodingExamples
	 */
	public static void encode(){
		JSONObject obj=new JSONObject();
//		obj.put("obj", new SampleClass())
		  obj.put("name","foo");
		  obj.put("num",new Integer(100));
		  obj.put("balance",new Double(1000.21));
		  obj.put("is_vip",new Boolean(true));
		  obj.put("nickname",null);
		 
		  System.out.println(obj.toJSONString());
	}
	
	/**
	 * https://code.google.com/p/json-simple/wiki/DecodingExamples
	 */
	public static void decode(){
		  String s="[0,{\"1\":{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}}]";
		  
		  Object obj=JSONValue.parse(s);
		  JSONArray array=(JSONArray)obj;
//		  System.out.println("======the 2nd element of array======");
//		  System.out.println(array.get(1));
//		  System.out.println();
		                
		  JSONObject obj2=(JSONObject)array.get(1);
//		  System.out.println("======field \"1\"==========");
//		  System.out.println(obj2.get("1"));    

		                
		  s="{}";
		  obj=JSONValue.parse(s);
//		  System.out.println(obj);
		                
		  s="[5,]";
		  obj=JSONValue.parse(s);
//		  System.out.println(obj);
		                
		  s="[5,,2]";
		  obj=JSONValue.parse(s);
//		  System.out.println(obj);
		
	}

}
