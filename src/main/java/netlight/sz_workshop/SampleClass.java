package netlight.sz_workshop;

import java.io.Serializable;

import org.codehaus.jackson.map.SerializationConfig;

public class SampleClass implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 467931316932296633L;
	
	
	public SampleClass(int a, double b, String[] c, SampleClass d) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
	
	public SampleClass() {
		super();
		this.a = 1;
		this.b = 2.;
		this.c = new String[]{"Hallo", "SZ"};
		this.d = null;
	}
	
	int a;
	double b;
	String[] c;
	SampleClass d;
	
	

}
