package netlight.sz_workshop.jsonparser;

import java.io.IOException;

import netlight.sz_workshop.SampleClass;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class Jackson {
	static ObjectMapper mapper = new ObjectMapper();
	
	public static String encode(SampleClass sc_) throws JsonGenerationException, JsonMappingException, IOException{
		mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
		return mapper.writeValueAsString(sc_);
	}
	
	public static SampleClass decode(String s) throws JsonGenerationException, JsonMappingException, IOException{
		mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
		return mapper.readValue(s, SampleClass.class);
		
	}
}
